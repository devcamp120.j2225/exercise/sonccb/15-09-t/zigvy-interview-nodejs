const photoModel = require('../models/photoModel');

const mongoose = require("mongoose");

const createPhoto = (req, res) => {
    //B1 thu thập dữ liệu
    let bodyReq = req.body;
    //B2 validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(bodyReq.albumId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId is invalid"
        })
    }
    if (!bodyReq.title) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "title is required"
        })
    }
    if (!bodyReq.url) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "url is required"
        })
    }
    if (!bodyReq.thumbnailUrl) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "thumbnailUrl is required"
        })
    }

    //B3 xử lý và trả về kết quả
    const newPhoto = {
        _id: mongoose.Types.ObjectId(),
        albumId: bodyReq.albumId,
        title: bodyReq.title,
        url: bodyReq.url,
        thumbnailUrl: bodyReq.thumbnailUrl
    }

    photoModel.create(newPhoto, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        } else {
            res.status(201).json({
                status: "Created photo successfully!",
                data: data
            })
        }
    })
}

const getAllPhoto = (req, res) => {
    //B1 thu thập dữ liệu
    let albumId = req.query.albumId; //GET "/photos?albumId=:albumId"

    //B2: validate dữ liệu
    let condition = {};

    if (albumId) {
        condition.albumId = albumId;
    }
    //B3: xử lý và trả về kết quả
    photoModel.find(condition, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        } else {
            res.status(200).json({
                status: "Get All Photo successfully !!!",
                data: data
            })
        }
    })
}

const getPhotoById = (req, res) => {
    //B1 thu thập dữ liệu
    let photoId = req.params.photoId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "photoId is invalid"
        })
    }
    //B3: xử lý và trả về kết quả
    photoModel.findById(photoId, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        } else {
            res.status(200).json({
                status: "Get Photo By Id successfully!!!",
                data: data
            })
        }
    })
}

const updatePhotoById = (req, res) => {
    //B1 thu thập dữ liệu
    let photoId = req.params.photoId;
    let bodyReq = req.body;
    //B2 validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        return res.status(400).json({
            message: "photoId in invalid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(bodyReq.albumId)) {
        return res.status(400).json({
            message: "albumId is invalid"
        })
    }
    if (!bodyReq.title) {
        return res.status(400).json({
            message: "title is required"
        })
    }
    if (!bodyReq.url) {
        return res.status(400).json({
            message: "url is required"
        })
    }
    if (!bodyReq.thumbnailUrl) {
        return res.status(400).json({
            message: "thumbnailUrl is required"
        })
    }
    //B3 xử lý dữ liệu, trả về kết quả
    let updatePhoto = {
        albumId: bodyReq.albumId,
        title: bodyReq.title,
        url: bodyReq.url,
        thumbnailUrl: bodyReq.thumbnailUrl
    }
    photoModel.findByIdAndUpdate(photoId, updatePhoto, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        } else {
            res.status(200).json({
                status: "Updated Photo !",
                data: data
            })
        }
    })
}

const deletePhotoById = (req, res) => {
    //B1: thu thập dữ liệu
    let photoId = req.params.photoId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(photoId)) {
        res.status(400).json({
            status: "Error 400: Bad request!",
            message: "photoId is invalid"
        })
    }
    //B3: xử lý và trả về kết quả
    photoModel.findByIdAndDelete(photoId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        } else {
            res.status(204).json({
                status: "Deleted Photo "
            })
        }
    })
}


const getPhotoOfAlbum = (req, res) => {
    //B1 thu thập dữ liệu
    let albumId = req.params.albumId;

    let condition = {};
    //B2 validate dữ liệu
    if (albumId) {
        condition.albumId = albumId;
    }
    //B3 xử lý và trả về kết quả
    photoModel.find(condition, (err, data) => {
        if (err) {
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        } else {
            res.status(200).json({
                status: "Get All Photo of album successfully !!!",
                data: data
            })
        }
    })
}

module.exports = { createPhoto, getAllPhoto, getPhotoById, updatePhotoById, deletePhotoById, getPhotoOfAlbum }