const mongoose = require('mongoose')
const commentModel = require('../models/commentModel')

const createComment = (request,res) => {
    //B1: thu thập dữ liệu
    let bodyRequest = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(bodyRequest.postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    if(!bodyRequest.name) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is required"
        })
    }
    if(!bodyRequest.email) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is required"
        })
    }
    if(!bodyRequest.body) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is required"
        })
    }
    
    //B3: xử lý dữ liệu và trả về kết quả
    const newComment = {
        _id: mongoose.Types.ObjectId(),
        postId: bodyRequest.postId,
        name: bodyRequest.name,
        email: bodyRequest.email,
        body: bodyRequest.body,
    }

    commentModel.create(newComment, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            return res.status(201).json({
                status: "Create comment successfully !!!",
                data: data
            })
        }
    }) 
}

const getAllComment = (req, res) => {
    //B1: thu thập dữ liệu
    let postId = req.query.postId; //GET “/comments?postId=:postId”
    //B2: validate dữ liệu
    let condition = {};

    if(postId){
        condition.postId = postId;
    }
    //B3: xử lý dữ liệu và trả về kết quả
    commentModel.find(condition, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            return res.status(200).json({
                status: "Get All Comment successfully !!!",
                data: data
            })
        }
    })
}

const getAllCommentById = (req, res) => {
    //B1: thu thập dữ liệu
    let commentId = req.params.commentId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "commentId is invalid"
        })
    }
    //B3: xử lý dữ liệu và trả về kết quả
    commentModel.findById(commentId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            return res.status(200).json({
                status: "Get Comment By Id successfully !!!",
                data: data
            })
        }
    })
}

const updateCommentById = (req, res) => {
    //B1 thu thập dữ liệu
    let commentId = req.params.commentId;
    let bodyRequest = req.body;
    //B2 validate dữ liệu
    if(commentId && !mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "commentId in invalid"
        })
    }
    if(bodyRequest.postId && !mongoose.Types.ObjectId.isValid(bodyRequest.postId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "postId is invalid"
        })
    }
    if(bodyRequest.name && !isNaN(bodyRequest.name)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "name is require String!"
        })
    }
    if(bodyRequest.email && !isNaN(bodyRequest.email)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "email is require String!"
        })
    }
    if(!bodyRequest.body && !isNaN(bodyRequest.body)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "body is require String!"
        })
    }
    //B3 xử lý data và trả về kết quả
    let updateComment = {
        postId: bodyRequest.postId,
        name: bodyRequest.name,
        email: bodyRequest.email,
        body: bodyRequest.body,
    }
    commentModel.findByIdAndUpdate(commentId, updateComment,  (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            return res.status(200).json({
                status: "Update Comment success!!!",
                data: data
            })
        }
    })
}

const deleteCommentById = (req, res) => {
    //B1: thu thập dữ liệu
    let commentId = req.params.commentId; 
    //B2 validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return res.status(400).json({
            status: "Error 400: Bad request!",
            message: "commentId is invalid"
        })
    }
    //B3 xử lý và trả về kết quả
    commentModel.findByIdAndDelete(commentId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            return res.status(204).json({
                status: "Delete Comment successfully !"
            })
        }
    })
}


const getCommentOfPost = (req, res) => {
    //B1 thu thập dữ luệu
    let postId = req.params.postId;

    let condition = {};
    //B2 validate dữ liệu
    if(postId){
        condition.postId = postId;
    }
    //B3 xử lý và trả về kết quả
    commentModel.find(condition, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            return res.status(200).json({
                status: "Get All Comment successfully !!!",
                data: data
            })
        }
    })
}

module.exports = {createComment, getAllComment, getAllCommentById, updateCommentById, deleteCommentById, getCommentOfPost}
