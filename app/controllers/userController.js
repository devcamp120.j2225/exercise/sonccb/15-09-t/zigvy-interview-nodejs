const mongoose = require('mongoose')

const userModel = require('../models/userModel')

const createUser = (request, response) => {
    //B1: thu thập dữ liệu
    let bodyRequest = request.body
    //B2: validate dữ liệu
    if (!bodyRequest.name) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "name is required"
        })
    }
    if (!bodyRequest.username) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "username is required"
        })
    }
    if (!bodyRequest.address) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "address is required"
        })
    }
    if (!bodyRequest.phone) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "phone is required"
        })
    }
    if (!bodyRequest.website) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "website is required"
        })
    }
    if (!bodyRequest.company) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "company is required"
        })
    }
    //B3: xử lý dữ liệu và hiển thị kết quả
    let createuser = {
        _id: mongoose.Types.ObjectId(),
        name: bodyRequest.name,
        username: bodyRequest.username,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
        website: bodyRequest.website,
        company: bodyRequest.company
    }
    userModel.create(createuser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: Create user success",
                data: data
            })
        }
    })
}

const getAllUser = (request, response) => {
    //b1: thu thập dữ liệu
    //b2: validate dữ liệu
    //b3: xử lý và trả ra kết quả
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        }
        else {
            return response.status(200).json({
                message: "successfully get all users",
                data: data
            })
        }
    })
}

// LẤY USER THEO ID
const getUserById = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "User ID is not valid"
        })
    }

    // B3: Thao tắc với cơ sở dữ liệu
    userModel.findById(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user by id success",
                data: data
            })
        }
    })
}


//UPDATE A user
const updateUserById = (request, response) => {
    // B1: Thu thập dữ liệu
    let userId = request.params.userId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "User ID is not valid"
        })
    }

    if (bodyRequest.name  && !isNaN(bodyRequest.name)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "name is require String!"
        })
    }
    if (!bodyRequest.username && !isNaN(bodyRequest.username)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "username is require String!"
        })
    }
    if (!bodyRequest.address && !isNaN(bodyRequest.address)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "address is require String!"
        })
    }
    if (!bodyRequest.phone && !isNaN(bodyRequest.phone)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "phone is require String!"
        })
    }
    if (!bodyRequest.website && !isNaN(bodyRequest.website)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "website is require String!"
        })
    }
    if (!bodyRequest.company && !isNaN(bodyRequest.company)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "company is require String!"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let updateUser = {
        name: bodyRequest.name,
        username: bodyRequest.username,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
        website: bodyRequest.website,
        company : bodyRequest.company
    }
    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update User success",
                data: data
            })
        }
    })
}


// DELETE A user
const deleteUserById = (request, response) => {
    // B1: thu thập dữ liệu
    let userId = request.params.userId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "User Id is not valid"
        })
    }

    //B3: Thao tắc với cơ sở dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: `Successfully delete userId ${userId}`,
            })
        }
    })
}

module.exports = { createUser, getAllUser, getUserById, updateUserById, deleteUserById }