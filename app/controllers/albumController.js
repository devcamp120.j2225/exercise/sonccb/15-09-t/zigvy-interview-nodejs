const mongoose = require('mongoose')

const albumModel = require('../models/albumModel')

const createAlbum = (req, res) => {
    //B1 thu thập dữ liệu
    let bodyReq = req.body;
    //B2 validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(bodyReq.userId)) {
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "userId is invalid"
        })
    }
    if(!bodyReq.title) {
        return res.status(400).json({
            message: "title is required"
        })
    }
   
    //B3: xử lý và trả về kết quả
    const newAlbum = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyReq.userId,
        title: bodyReq.title
       
    }

    albumModel.create(newAlbum, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Created album successfully !!!",
                data: data
            })
        }
    }) 
}

const getAllAlbum = (req, res) => {
    //B1 thu thập dữ liệu
    let userId = req.query.userId;
    //B2: validate dữ liệu
    let condition = {}; //GET “/albums?userId=:userId

    if(userId){
        condition.userId = userId;
    }
    //B3 xử lý và trả về kết quả
    albumModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Album successfully !!!",
                data: data
            })
        }
    })
}

const getAlbumById = (req, res) => {
    //B1 thu thập dữ liệu
    let albumId = req.params.albumId;
    //B2 validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "albumId is invalid"
        })
    }
    //B3 xử lý và trả về dữ liệu
    albumModel.findById(albumId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get Album By Id successfully !!!",
                data: data
            })
        }
    })
}

const updateAlbumById = (req, res) => {
    //B1 thu thập dữ liệu
    let albumId = req.params.albumId;
    let bodyReq = req.body;
    //B2 validate dữ liệu
    if(albumId && !mongoose.Types.ObjectId.isValid(albumId)){
        return res.status(400).json({
            message: "albumId in invalid"
        })
    }
    if( bodyReq.userId && !mongoose.Types.ObjectId.isValid(bodyReq.userId)) {
        return res.status(400).json({
            message: "userId is invalid"
        })
    }
    if( bodyReq.title && !isNaN(bodyReq.title)) {
        return res.status(400).json({
            message: "title is require String!"
        })
    }
    //B3 xử lý và trả về kết quả
    let updateAlbum = {
        userId: bodyReq.userId,
        title: bodyReq.title,
    }
    albumModel.findByIdAndUpdate(albumId, updateAlbum,  (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Update Album successfully !!!",
                data: data
            })
        }
    })
}

const deleteAlbumById = (req, res) => {
    //B1: thu thập dữ liệu
    let albumId = req.params.albumId; 
    //B2 validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(albumId)){
        res.status(400).json({
            status: "Error 400: Bad request!",
            message: "albumId is invalid"
        })
    }
    //B3: xử lý và trả về kết quả
    albumModel.findByIdAndDelete(albumId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                message: `Deleted Album ${albumId}`
            })
        }
    })
}


const getAlbumOfUser = (req, res) => {
    //B1 thu thập dữ liệu
   let userId = req.params.userId

    let condition = {};
    //B2 validate dữ liệu
    if(userId){
        condition.userId = userId;
    }
    //B3 xử lý dữ liệu và trả về kết quả
    albumModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Album of user successfully!!!",
                data: data
            })
        }
    })
}

module.exports = {createAlbum, getAllAlbum, getAlbumById, updateAlbumById, deleteAlbumById, getAlbumOfUser}