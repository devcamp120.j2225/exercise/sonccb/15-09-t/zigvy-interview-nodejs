const todoModel = require('../models/todoModel');

const mongoose = require("mongoose");

const createTodo = (req, res) => {
    //B1: thu thập dữ liệu
    let bodyReq = req.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(bodyReq.userId)) {
        return res.status(400).json({
            message: "userId is invalid"
        })
    }
    if(!bodyReq.title) {
        return res.status(400).json({
            message: "title is required"
        })
    }
    if(!bodyReq.completed) {
        return res.status(400).json({
            message: "complete status is required"
        })
    }
   
    //B3: xử lý và trả về kết quả
    const newToDo = {
        _id: mongoose.Types.ObjectId(),
        userId: bodyReq.userId,
        title: bodyReq.title,
        completed: bodyReq.completed
    }

    todoModel.create(newToDo, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(201).json({
                status: "Created new Todo !!!",
                data: data
            })
        }
    }) 
}

const getAllTodo = (req, res) => {
    //B1 thu thập dữ liệu
    let userId = req.query.userId;   //GET "/todos?userId=:userId"
    //b2: validate dữ liệu
    let condition = {};

    if(userId){
        condition.userId = userId;
    }
    //B3 xử lý và trả về kết quả
    todoModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get All Todo successfully !!!",
                data: data
            })
        }
    })
}

const getTodoById = (req, res) => {
    //B1 thu thập dữ liệu
    let todoId = req.params.todoId;
    //B2 validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "todoId is invalid"
        })
    }
    //B3 xử lý và trả về kết quả
    todoModel.findById(todoId, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get Todo By Id successfully !!!",
                data: data
            })
        }
    })
}

const updateTodoById = (req, res) => {
    //B1 thu thập dữ liệu
    let todoId = req.params.todoId;
    let bodyReq = req.body;
    //B2 validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        return res.status(400).json({
            status: "Error 400: Bad request",
            message: "todoId in invalid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(bodyReq.userId)) {
        return res.status(400).json({
            message: "userId is invalid"
        })
    }
    if(!bodyReq.title) {
        return res.status(400).json({
            message: "title is required"
        })
    }
    if(!bodyReq.completed) {
        return res.status(400).json({
            message: "complete status is required"
        })
    }
    //B3 xử lý và trả về kết quả
    let updateTodo = {
        userId: bodyReq.userId,
        title: bodyReq.title,
        completed: bodyReq.completed
    }
    todoModel.findByIdAndUpdate(todoId, updateTodo,  (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Update Todo successfully !",
                data: data
            })
        }
    })
}

const deleteTodoById = (req, res) => {
    //B1: thu thập dữ liệu
    let todoId = req.params.todoId; 
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(todoId)){
        res.status(400).json({
            status: "Error 400: Bad request!",
            message: "todoId is invalid"
        })
    }
    //B3: xử lý và trả về kết quả
    todoModel.findByIdAndDelete(todoId, (err, data) => {
        if(err){
            return res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(204).json({
                status: "Deleted Todo by Id"
            })
        }
    })
}


const getTodoOfUser = (req, res) => {
    //B1 thu thập thông tin
    let userId = req.params.userId;

    let condition = {};
    //B2 validate thông tin
    if(userId){
        condition.userId = userId;
    }
    //B3 xử lý và trả về kết quả
    todoModel.find(condition, (err, data) => {
        if(err){
            res.status(500).json({
                status: "Error 500: Internal server error",
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Successfully get All Todo of user !",
                data: data
            })
        }
    })
}

module.exports = {createTodo, getAllTodo, getTodoById, updateTodoById, deleteTodoById, getTodoOfUser}