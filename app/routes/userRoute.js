const express = require('express')
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require('../controllers/userController')

const userRouter = express.Router()

userRouter.post('/users', createUser)

userRouter.get('/users', getAllUser)

userRouter.get('/users/:userId', getUserById)

userRouter.put('/users/:userId', updateUserById)

userRouter.delete('/users/:userId', deleteUserById) 
module.exports = {userRouter}