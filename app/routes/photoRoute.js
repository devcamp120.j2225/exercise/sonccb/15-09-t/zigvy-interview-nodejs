const express = require('express');

const photoRouter = express.Router();

const {createPhoto, getAllPhoto, getPhotoById, updatePhotoById, deletePhotoById, getPhotoOfAlbum} = require('../controllers/photoController');

photoRouter.get('/photos', getAllPhoto);
photoRouter.post('/photos', createPhoto);
photoRouter.get('/photos/:photoId', getPhotoById);
photoRouter.get('/albums/:albumId/photos', getPhotoOfAlbum);

photoRouter.put('/photos/:photoId', updatePhotoById);
photoRouter.delete('/photos/:photoId', deletePhotoById);

module.exports = {photoRouter}