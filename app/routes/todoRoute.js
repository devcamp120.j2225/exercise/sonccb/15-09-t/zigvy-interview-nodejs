const express = require('express');

const todoRouter = express.Router();

const {createTodo, getAllTodo, getTodoById, updateTodoById, deleteTodoById, getTodoOfUser} = require('../controllers/todoController');

todoRouter.get('/todos', getAllTodo);
todoRouter.post('/todos', createTodo);
todoRouter.get('/todos/:todoId', getTodoById);
todoRouter.get('/users/:userId/todos', getTodoOfUser);

todoRouter.put('/todos/:todoId', updateTodoById);
todoRouter.delete('/todos/:todoId', deleteTodoById);

module.exports = todoRouter