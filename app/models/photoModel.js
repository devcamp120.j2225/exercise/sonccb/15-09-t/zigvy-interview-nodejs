const mongoose = require('mongoose');

const Schema = mongoose.Schema

const photoSchema = new Schema ({
    _id: mongoose.Types.ObjectId,
    albumId: {
        type: mongoose.Types.ObjectId,
        ref: 'album',
        require: true
    },
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    thumbnailUrl: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('photo', photoSchema)